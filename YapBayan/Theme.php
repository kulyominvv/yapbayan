<?php
namespace YapBayan;

class Theme{
	
	private $id;
	private $name;
	private $rating;
	private $views;
	private $author;
	private $author_id;
	private $coubs_count = 0;
	
	function __construct($id = 0, $name = "", $rating = 0, $views = 0, $author = "", $author_id = 0){
		$this->id = $id;
		$this->name = $name;
		$this->rating = $rating;
		$this->views = $views;
		$this->author = $author;
		$this->author_id = $author_id;
	}
	
	function set($fields){
		foreach($fields as $field_name=>$value)
			$this->$field_name = trim($value);
	}
	
	function setId($id){
		$this->id = $id;
	}
	
	function setName($name){
		$this->name = $name;
	}
	
	function setRating($rating){
		$this->rating = (int) trim($rating);
	}
	
	function setViews($views){
		$this->views = $views;
	}
	
	function setAuthor($author){
		$this->author = $author;
	}
	
	function setAuthorId($author_id){
		$this->author_id = $author_id;
	}
	
	function getId(){
		return $this->id;
	}
	
	function getName(){
		return $this->name;
	}
	
	function getRating(){
		return $this->rating;
	}
	
	function getViews(){
		return $this->views;
	}
	
	function getAuthor(){
		return $this->author;
	}
	
	function getAuthorId(){
		return $this->author_id;
	}
	
	function plusCoub(){
		$this->coubs_count++;
	}
	
	function getCoubsCount(){
		return $this->coubs_count;
	}
	
	function asArray(){
		return array("id"=>$this->id,"name"=>$this->name,"rating"=>$this->rating,"views"=>$this->views,"author"=>$this->author,"author_id"=>$this->author_id);
	}	
}

?>