<?php 
namespace YapBayan;

use Goutte\Client;
use Flatbase\Storage\Filesystem;
use Flatbase\Flatbase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\CssSelector\CssSelectorConverter;

class BayanSearch{
	
	private $authCookies = null;
	public $db = null;
	public $theme_id;
	public $root;
	
	
	function __construct(){
		
		ob_implicit_flush(true);
		
		$root = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR;
		$this->root = $root;
		
		$storage = new Filesystem($root.'storage/');		
		$this->db = new Flatbase($storage);
		
		$this->autorize();				
	}
	
	function autorize(){
		$client = new Client();
		$crawler = $client->request('GET', 'https://www.yaplakal.com/act/Login/CODE/01/');
		
		$form = $crawler->selectButton('Войти')->form();
		$form['UserName'] = '';
		$form['PassWord'] = '';
		
		$crawler = $client->submit($form);
		
		$this->authCookies = $client->getCookieJar();				
	}
	
	function closest(Crawler $crawler, $cssSelector){
		$xpath = (new CssSelectorConverter())->toXPath($cssSelector, './');
		$closest = null;
		for(
			$domNode = $crawler->getNode(0);
			$domNode !== null && $domNode->nodeType === XML_ELEMENT_NODE;
			$domNode = $domNode->parentNode
			) {
				$subcrawler = new Crawler($domNode);
				$subcrawlerFiltered = $subcrawler->filterXPath($xpath);
				if (count($subcrawlerFiltered) > 0) {
					$closest = $subcrawlerFiltered;
					break;
				}
			}
			
		if ($closest === null)
			$closest = new Crawler();
					
		return $closest;
	}
	
	function saveCoubTheme($theme, $uniq = true){
		if($uniq)
			$this->db->delete()->in('theme_links')->where('id', '==', $theme['id'])->execute();
			
			$this->db->insert()->in('theme_links')->set($theme)->execute();
	}
	
	function saveCoubsInTheme($coub_info, $uniq = true){		
		if($uniq)
			$this->db->delete()->in('coubs_in_theme')->where('msg_id', '==', $coub_info['msg_id'])->execute();
		
		$this->db->insert()->in('coubs_in_theme')->set($coub_info)->execute();					
	}
	
	function getCoubThemesFromDb(){
		return $this->db->read()->in('theme_links')->get();
	}
	
	function getCoubsInThemeFromDb($theme_id){
		return $this->db->read()->in('coubs_in_theme')->where('theme_id', '==', $theme_id)->get();
	}
	
	function parseThemeByPage(Client &$client, Crawler $crawler, $offset = 0){
		
		$links = $crawler->filter('#ipbwrapper .tablebasic tr')->each(function($tr,$i){
			
			if($i == 0)
				return;
				
				$link= $tr->filter('a.subtitle');
				$theme_id = explode('?', $link->attr('href'));
				$theme_id = explode('/',$theme_id[0]);
				$rating = $tr->filter('.rating-short-value');
				$views = $tr->filter('td:nth-child(7)');
				$author = $tr->filter('td:nth-child(5)')->filter('a');
				
				$author_id = explode('/',$author->attr('href'));
				
				//тема с кубами за пол года
				if(end($theme_id)== 'topic1822595.html')
					return;
				
				$theme_data = array(
						"id" => str_replace('.html', '', end($theme_id)),
						"name" => $link->text(),
						"rating" => $rating->text(),
						"views" => $views->text(),
						"author"=> $author->text(),
						"author_id" => str_replace('.html', '', end($author_id)),
				);

				$this->saveCoubTheme($theme_data);
		});
		
		if(count($links) == 26){			
			$url_params = array();
			$offset+=25;
			
			$current_url = urldecode($crawler->getUri());			
			$url_parts = parse_url($current_url);			
			parse_str($url_parts['query'],$url_params);
						
			$next_page_url = $url_parts['scheme'].'://'.$url_parts['host'].$url_parts['path'].'?act='.$url_params['act'].'&CODE='.$url_params['CODE'].'&searchid='.$url_params['searchid'].'&search_in='.$url_params['search_in'].'&st='.$offset;
									
			$crawler = $client->request('GET', $next_page_url);	
			
			$this->parseThemeByPage($client,$crawler,$offset);
		}		
	}
	
	function parseCoubsByPage(Client &$client, Crawler $crawler, $theme_id, $offset = 0, $bef_url = ''){
		
		$current_url = urldecode($crawler->getUri());
				
		if($bef_url == $current_url)
			return;
		
		$this->writeFile('=========================START PARSE PAGE #'.$current_url.'=========================');
		print(date('H:i:s').' START PARSE PAGE '.$current_url."\n");
			
		$coubs = $crawler->filter('#content-inner iframe')->each(function($iframe,$i){
			$frame_src = $iframe->attr('src');
			
			$coub_info = array();
			
			if(strpos($frame_src,'//coub.com/embed/')!==false){
				
				$table = $this->closest($iframe,'table');
				
				$msg_id = $table->attr('id');
				
				$date = $table->filter('.desc.post-tools a')->first();
				$author = $table->filter('.desc:not(.post-tools) a')->first();
				$rating = $table->filter('.post-rank span')->first();
				
				if(!count($author)){
					return;
				}
				
				$author_id = explode('/',$author->attr('href'));
				$coub_id = explode('/',$frame_src);
				
				$coub_info['msg_id'] = str_replace('p_row_', '', $msg_id);
				$coub_info['theme_id'] = $this->theme_id;
				$coub_info['coub_id'] = end($coub_id);
				$coub_info['author_id'] = str_replace('.html', '', end($author_id));
				$coub_info['posted'] = $date->text();
				$coub_info['rating'] = ($rating->count())?$rating->text():"";
				
				$this->saveCoubsInTheme($coub_info);
			}
		});
		
		$this->writeFile('=========================STOP PARSE PAGE #'.$current_url.'=========================');
		
		$offset+=25;
		$next_page_url = 'https://www.yaplakal.com/forum28/st/'.$offset.'/'.$theme_id.'.html'; 
		$crawler = $client->request('GET', $next_page_url);	
		$this->parseCoubsByPage($client, $crawler, $theme_id, $offset, $current_url);
	}
	
	function searchCoubThemes(){
		$themes = array();
		
		$client = new Client(array(), null, $this->authCookies);
		
		$period = 180;
		$keywords = 'кубики coub кубик кубы coubs coub\'s';
		$params = array('keywords'=>$keywords,'search_in'=>'titles','forums'=>array('28'),'searchsubs'=>'1','search_how'=>'any','sort_by'=>'date','prune'=>$period,'namesearch'=>'');
		
		$crawler = $client->request('POST', 'https://www.yaplakal.com/?act=Search&CODE=01',$params);		
		$this->parseThemeByPage($client, $crawler, 0);
	}
	
	function searchCoubsInTheme($theme_id){
		
		$this->writeFile('******************START PARSE THEME #'.$theme_id.'******************');
		
		print(date('H:i:s').' START PARSE THEME #'.$theme_id."\n");
		
		$this->theme_id = $theme_id;
		$client = new Client(array(), null, $this->authCookies);
		
		$crawler = $client->request('GET', 'https://www.yaplakal.com/forum28/'.$theme_id.'.html',array());
		
		$this->parseCoubsByPage($client, $crawler,$theme_id, 0, '');
		
		$this->writeFile('******************STOP PARSE THEME #'.$theme_id.'******************');
	}
	
	function parseAll($theme_count){
		$themes = $this->getCoubThemesFromDb();
				
		$i = 0;
		$j = 0;
		
		foreach($themes as $theme){
			$is_parsed = $this->db->read()->in('parsed_theme')->where('theme_id', '==', $theme['id']);
			
			if(!$is_parsed->count()){				
				$this->searchCoubsInTheme($theme['id']);
				$this->db->insert()->in('parsed_theme')->set(array('theme_id'=>$theme['id']))->execute();
				$i++;
			}
			else{
				$j++;
			}
			
			if($i == $theme_count)
				break;						
		}
		
		if($i > 0)			
			print('Parsed '.($j+$i).' of '.count($themes).' themes');					
		else
			print('All themes parsed');				
	}
	
	function writeFile($msg){
		$file = $this->root.'log.txt';
		
		$content = ""; 
		
		if(file_exists($file))
			$content = file_get_contents($file);
				
		$content .= date('d.m.Y H:i:s');
		$content .= ' | '.$msg;
		$content .= "\n";
		file_put_contents($file, $content);
	}
}

?>