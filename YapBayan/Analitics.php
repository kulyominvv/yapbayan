<?php
namespace YapBayan;

use Flatbase\Storage\Filesystem;
use Flatbase\Flatbase;

class Analitics{
	
	private $db = null;
	
	public $coubs = array();
	public $themes = array();
	public $authors = array();
	
	function __construct(){
		$root = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR;
		
		$storage = new Filesystem($root.'storage/');
		$this->db = new Flatbase($storage);
		
		$themes = $this->db->read()->in('theme_links')->execute()->getArrayCopy();
		foreach($themes as $theme){
			$theme_obj = new Theme();
			$theme_obj->set($theme);
			$this->themes[$theme_obj->getId()] = $theme_obj;
			
			if(isset($this->authors[$theme_obj->getAuthorId()])){
				$this->authors[$theme_obj->getAuthorId()]->plusTheme();
				$this->authors[$theme_obj->getAuthorId()]->addThemeUq($theme_obj->getRating());
			}
			else{
				$author = new Author();				
				$author->setId($theme_obj->getAuthorId());
				$author->setName($theme_obj->getAuthor());
				$author->plusTheme();
				$author->addThemeUq($theme_obj->getRating());
				$this->authors[$theme_obj->getAuthorId()] = $author;				
			}
		}
				
		$coubs = $this->db->read()->in('coubs_in_theme')->execute()->getArrayCopy();		
		foreach($coubs as $coub){
			$coub_obj = new Coub();
			$coub_obj->set($coub);
			$this->coubs[$coub_obj->getMsgId()] = $coub_obj;
			
			$this->themes[$coub_obj->getThemeId()]->plusCoub();
			
			if(isset($this->authors[$coub_obj->getAuthorId()])){
				$this->authors[$coub_obj->getAuthorId()]->plusCoub();
				$this->authors[$coub_obj->getAuthorId()]->addCoubUq($coub_obj->getRating());
			}
			else{
				$author = new Author();
				$author->setId($coub_obj->getAuthorId());				
				$author->plusCoub();
				$author->addCoubUq($coub_obj->getRating());
				$this->authors[$coub_obj->getAuthorId()] = $author;				
			}						
		}
		
		foreach($this->themes as $theme_id => $theme){
			if($theme->getCoubsCount() == 0)
				unset($this->themes[$theme_id]);
		}
	}
			
	function themesCount(){
		return count($this->themes);
	}
	
	function coubsCount(){
		return count($this->coubs);
	}
	
	function coubsInThemeCount(string $theme_id){
		
		$coubs_in_theme = 0;
		
		foreach($this->coubs as $coub){
			if($coub['theme_id'] == $theme_id)
				$coubs_in_theme++;
		}
				
		return $coubs_in_theme;
	}
	
	function findMaxCoubsInTheme(){
		
		$find_theme = new Theme();
		
		foreach($this->themes as $theme){
			if($find_theme->getCoubsCount() < $theme->getCoubsCount())
				$find_theme = $theme;
		}
		
		return 'Тема с самым большим колличеством кубов: '.$find_theme->getId().'. Всего кубов: '.$find_theme->getCoubsCount().'. Рейтинг: '.$find_theme->getRating().'. Автор: '.$find_theme->getAuthor();		
	}
	
	function findMaxRatingTheme(){
		$find_theme = new Theme();
		
		foreach($this->themes as $theme){
			if($find_theme->getRating() < $theme->getRating())
				$find_theme = $theme;
		}
		
		return 'Тема с самым большим рейтингом: '.$find_theme->getId().'. Всего кубов: '.$find_theme->getCoubsCount().'. Рейтинг: '.$find_theme->getRating().'. Автор: '.$find_theme->getAuthor();
	}
	
	function getTopAuthorByThemes($authors_count){
		$top_authors = array();
		
		foreach($this->authors as $author)
			$top_authors[$author->getId()] = $author->getThemesCount();
		
		
		arsort($top_authors, SORT_NUMERIC);
				
		$top_authors = array_slice($top_authors, 0, $authors_count);
		
		$st = '<div><strong>Топ '.$authors_count.' авторов по кол-ву созданных тем</strong></div>';
		
		$i=1;
		
		foreach($top_authors as $author_id => $themes_count){
			$st.='<div>'.$i.'. '.$this->authors[$author_id]->getId().' - '.$this->authors[$author_id]->getName().' - '.$themes_count.' Кубов довблено: '.$this->authors[$author_id]->getCoubsCount().' Заработано юки на темах: '.$this->authors[$author_id]->getThemesUq().'<div>';
			$i++;
		}
					
		return $st;		
	}
	
	function getTopAuthorByCoubs($authors_count){
		$top_authors = array();
		
		foreach($this->authors as $author)
			$top_authors[$author->getId()] = $author->getCoubsCount();
			
			
			arsort($top_authors, SORT_NUMERIC);			
			$top_authors = array_slice($top_authors, 0, $authors_count);
			
			$st = '<div><strong>Топ '.$authors_count.' авторов по кол-ву залитых кубов</strong></div>';
			
			$i=1;
			
			foreach($top_authors as $author_id => $coubs_count){
				$st.='<div>'.$i.'. '.$this->authors[$author_id]->getId().' - '.$this->authors[$author_id]->getName().' - '.$coubs_count.' Тем создано: '.$this->authors[$author_id]->getThemesCount().' Заработано юки на кубах: '.$this->authors[$author_id]->getCoubsUq().'<div>';
				$i++;
			}
			
			return $st;		
	}
	
	function getTopCoubsBayan($coubs_count){
		$top_coub_banyans = array();
		$total_bayans = 0;
		
		foreach($this->coubs as $coub){
			$coub_id = $coub->getCoubId();
			
			if(isset($top_coub_banyans[$coub_id]))
				$top_coub_banyans[$coub_id]++;
			else
				$top_coub_banyans[$coub_id] = 1;						
		}
		
		arsort($top_coub_banyans, SORT_NUMERIC);
		
		foreach($top_coub_banyans as $coub_id=>$coub_posted_count){
			if($coub_posted_count > 1)
				$total_bayans = $total_bayans + ($coub_posted_count - 1);
		}
		
		$top_coub_banyans = array_slice($top_coub_banyans, 0, $coubs_count);
		
		$i=1;				
		$st = '<div><strong>Топ '.$coubs_count.' самых баянистых кубов</strong></div>';
		
		foreach($top_coub_banyans as $coub_id=>$coub_posted_count){
			$st.='<div>'.$i.'. <a target="_blank" href="https://coub.com/view/'.$coub_id.'">'.$coub_id.'</a> - '.$coub_posted_count.'<div>';
			$i++;
		}
		
		$st .= '<div><strong>Всего залито баянистых кубов '.$total_bayans.'</strong> или '.number_format((($total_bayans*100)/$this->coubsCount()),2).'%</div>';
		
		return $st;
	}
	
	function getTopAuthorBayan($authors_count){
		
		$msg_coub = array();
		$top_author_bayan = array();
		
		foreach($this->coubs as $coub)
			$msg_coub[$coub->getMsgId()] = $coub->getCoubId();
		
		asort($msg_coub, SORT_NUMERIC);
		
		$uniq_coubs = array();
		
		foreach($msg_coub as $msg_id=>$coub_id){
			if(isset($uniq_coubs[$coub_id])){				
				$author_id = $this->coubs[$msg_id]->getAuthorId();
				$this->authors[$author_id]->plusBayanCoub();												
			}
			else{
				$uniq_coubs[$coub_id] = $msg_id;
			}
		}
		
		foreach($this->authors as $author_id => $author){
			
			$author->calcBayanCoubsPercent();
			
			if($author->getBayanCoubsCount() > 49)
				$top_author_bayan[$author_id] = $author->getBayanCoubsPercent();			
		}
		
		arsort($top_author_bayan, SORT_NUMERIC);
		$top_author_bayan = array_slice($top_author_bayan, 0, $authors_count);
		
		$i=1;
		$st = '<div><strong>Топ '.$authors_count.' самых баянистых авторов</strong></div>';
		
		foreach($top_author_bayan as $author_id=>$bayan_percent){
			$st.='<div>'.$i.'. '.$author_id.' - '.$this->authors[$author_id]->getName().' - '.$this->authors[$author_id]->getCoubsCount().' - '.$this->authors[$author_id]->getBayanCoubsCount().' - '.$bayan_percent.'%<div>';
			$i++;
		}
		
		return $st;		
	}
	
	function getTopAuthorCoubRating($authors_count){
		$top_author = array();
		
		foreach($this->authors as $author_id => $author){
			if($author->getCoubsCount() > 49)
				$top_author[$author_id] = $author->getCoubsUq()/$author->getCoubsCount();
		}
		
		arsort($top_author, SORT_NUMERIC);
		$top_author = array_slice($top_author, 0, $authors_count);
		
		$i=1;
		$st = '<div><strong>[b]Топ '.$authors_count.' авторов с самыми качественными кубами[/b]</strong></div>';
		$st .= '*данная метрика рассчитывается для авторов, запостивших более 50 кубов';
		$st .= '<br/>[list]';
		foreach($top_author as $author_id=>$mod_rating){
			$st.='<div>['.$i.'] [url=http://www.yaplakal.com/members/'.$author_id.'.html] [b]'.$this->authors[$author_id]->getName().'[/b][/url] - Всего кубов добавлено: '.$this->authors[$author_id]->getCoubsCount().'- Всего получено юки: '.$this->authors[$author_id]->getCoubsUq().' - Средний рейтинг куба: '.round($mod_rating).'<div>';
			$i++;
		}
		$st .= '[/list]';
		
		return $st;		
	}
	
	function getTopRatingsCoubs($coubs_count){
		$top_best_coubs = array();
		$top_worst_coubs = array();
		
		foreach($this->coubs as $coub){
			$coub_id = $coub->getCoubId();
			
			if(isset($top_best_coubs[$coub_id]))
				$top_best_coubs[$coub_id] += $coub->getRating();
			else
				$top_best_coubs[$coub_id] = $coub->getRating();				
		}
		
		$top_worst_coubs = $top_best_coubs;
		
		arsort($top_best_coubs, SORT_NUMERIC);
		$top_best_coubs = array_slice($top_best_coubs, 0, $coubs_count);
		
		$i=1;
		$st = '<div><strong>Топ '.$coubs_count.' кубов по набраной общей юки</strong></div>';
		
		foreach($top_best_coubs as $coub_id=>$coub_rating){
			$st.='<div>'.$i.'. <a target="_blank" href="https://coub.com/view/'.$coub_id.'">'.$coub_id.'</a> - '.$coub_rating.'<div>';
			$i++;
		}
		
		
		asort($top_worst_coubs, SORT_NUMERIC);
		$top_worst_coubs = array_slice($top_worst_coubs, 0, $coubs_count);
		
		$i=1;
		$st .= '<div><strong>Топ '.$coubs_count.' самых худших кубов</strong></div>';
		
		foreach($top_worst_coubs as $coub_id=>$coub_rating){
			$st.='<div>'.$i.'. <a target="_blank" href="https://coub.com/view/'.$coub_id.'">'.$coub_id.'</a> - '.$coub_rating.'<div>';
			$i++;
		}
		
		return $st;
	}
	
	function show(){		
		print('Всего тем: '.$this->themesCount().'<br/>');
		print('Всего кубов: '.$this->coubsCount().'<br/>');
		print($this->findMaxCoubsInTheme().'<br/>');
		print($this->findMaxRatingTheme().'<br/>');
		print($this->getTopAuthorByThemes(10).'<br/>');
		print($this->getTopAuthorByCoubs(10).'<br/>');
		print($this->getTopCoubsBayan(10).'<br/>');
		print($this->getTopRatingsCoubs(10).'<br/>');
		print($this->getTopAuthorBayan(10).'<br/>');
		print($this->getTopAuthorCoubRating(10).'<br/>');
	}
	
}
?>