<?php
namespace YapBayan;

class Coub{
	
	private $msg_id;
	private $theme_id;
	private $coub_id;	
	private $rating;		
	private $author_id;
	private $posted;
	
	function __construct($msg_id = 0, $theme_id = '', $coub_id= '', $author_id = 0, $posted = '', $rating = 0){
		$this->msg_id = $msg_id;
		$this->theme_id = $theme_id;
		$this->coub_id = $coub_id;							
		$this->author_id = $author_id;
		$this->posted= $posted;
		$this->rating = $rating;
	}
	
	public function set($fields){
		foreach($fields as $field_name=>$value)
			$this->$field_name = trim($value);
	}
	
	public function setMsgId($msg_id){
		$this->msg_id = $msg_id;
	}
	
	public function setThemeId($theme_id){
		$this->theme_id = $theme_id;
	}
	
	public function setCoubId($coub_id){
		$this->coub_id = $coub_id;
	}
	
	public function setRating($rating){
		$this->rating = (int)trim($rating);		
	}
		
	public function setAuthorId($author_id){
		$this->author_id = $author_id;
	}
	
	public function setPosted($posted){
		$this->posted = $posted;
	}
	
	public function getMsgId(){
		return $this->msg_id;
	}
	
	public function getThemeId(){
		return $this->theme_id;
	}
	
	public function getCoubId(){		
		return $this->coub_id;
	}
	
	public function getRating(){		
		return (int)$this->rating;
	}
	
	public function getAuthorId(){
		return $this->author_id;
	}
	
	public function getPosted(){
		return $this->posted;
	}
	
	public function asArray(){
		return array(
				"msg_id"=>$this->msg_id,
				"theme_id"=>$this->theme_id,
				"coub_id"=>$this->coub_id,
				"rating"=>$this->rating,				
				"author_id"=>$this->author_id,
				"posted"=>$this->posted			
		);
	}
	
	public function inTheme($theme_id){
		return ($this->theme_id == $theme_id)?true:false;
	}
}

?>