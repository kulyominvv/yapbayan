<?php 
namespace YapBayan;

class Author{
	
	private $author_id;
	private $author_name;	
	private $themes_count = 0;
	private $coubs_count = 0;
	private $themes_uq = 0;
	private $coubs_uq = 0;
	private $bayan_coubs_count = 0;
	private $bayan_coubs_percent = 0;
	
	function setId($author_id){
		$this->author_id = $author_id;
	}
	
	function setName($author_name){
		$this->author_name = $author_name;
	}
	
	function getId(){
		return $this->author_id;
	}
	
	function getName(){
		return $this->author_name;
	}
	
	function plusTheme(){
		$this->themes_count++;
	}
	
	function plusCoub(){
		$this->coubs_count++;
	}
	
	function plusBayanCoub(){
		$this->bayan_coubs_count++;
	}
	
	function getThemesCount(){
		return $this->themes_count;
	}
	
	function getCoubsCount(){
		return $this->coubs_count;
	}
	
	function getBayanCoubsCount(){
		return $this->bayan_coubs_count;
	}
	
	function addThemeUq(int $uq){		
		$this->themes_uq += $uq;
	}
	
	function getThemesUq(){
		return $this->themes_uq;
	}
	
	function addCoubUq(int $uq){		
		$this->coubs_uq += $uq;
	}
	
	function getCoubsUq(){
		return $this->coubs_uq;
	}
	
	function calcBayanCoubsPercent(){
		if($this->coubs_count > 0)					
			$this->bayan_coubs_percent = round(($this->bayan_coubs_count/$this->coubs_count)*100);
		else 
			return 0;
	}
	
	function getBayanCoubsPercent(){
		return $this->bayan_coubs_percent;
	}
}
?>