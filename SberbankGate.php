<?php

namespace Etherra;

class SberbankGate{
	
	private $userName = "";
	private $pass = "";
	
	protected $server = "https://3dsec.sberbank.ru/payment/rest/";

	function __construct(){
		
	}
	
	function getEndPoint($for){
		$endpoint = "";
		
		switch($for){
			case "registerOrder": $endpoint = "register.do"; break; //Регистрация заказа
			case "preAuthRegistration": $endpoint = "registerPreAuth.do"; break; //Регистрация заказа с предавторизацией
			case "closeOrder": $endpoint = "deposit.do"; break; //Запрос завершения оплаты заказа
			case "cancelOrder": $endpoint = "reverse.do"; break; //Запрос отмены оплаты заказа
			case "returnMoney": $endpoint = "refund.do"; break; //Запрос возврата средств оплаты заказа
			case "orderStatus": $endpoint = "getOrderStatusExtended.do"; break; //Получение статуса заказа
			case "is3ds": $endpoint = "verifyEnrollment.do"; break; //Запрос проверки вовлеченности карты в 3DS
			case "paymentOrderBinding": $endpoint = "paymentOrderBinding.do"; break; //Запрос проведения оплаты по связкам
			case "unBindCard": $endpoint = "unBindCard.do"; break; //Запрос деактивации связки
			case "bindCard": $endpoint = "bindCard.do"; break; //Запрос активации связки
			case "extendBinding": $endpoint = "extendBinding.do"; break; //Запрос изменения срока действия связки
			case "getBindings": $endpoint = "getBindings.do"; break; //Запрос списка всех связок клиента
			case "getBindingsByCard": $endpoint = "getBindingsByCardOrId.do"; break; //Запрос списка связок определённой банковской карты
		}
		
		return $endpoint;
	}
	
	function getPaymentUrl(&$order_id){
		
		$paymentUrl = "";
		$paymentInfo = array();
		$data = array();
		
		//проверка на существование транзакции для данного заказа
		//если транзакция существует и истекла, меняем номер заказа
		$paymentInfo = $this->getOrderStatus($order_id);
		
		//данные не получены
		if(!isset($paymentInfo['errorCode'])){
			$paymentUrl = '/programms/discount/?errorCode=-1';
			return $paymentUrl;
		}
		//6 - заказ не найден в системе сбербанка, во всех остальных случаях транзакция уже существует и нужно менять order_id
		elseif($paymentInfo['errorCode'] == 6){
			Db::execute('UPDATE programms_orders SET total_transactions = total_transactions+1 WHERE id=?',$order_id);
		}
		else{
			//обновляем номер заказа для регистрации в платежном шлюзе
			
			$max_order_id = Db::selectScalar("SELECT max(id) FROM programms_orders");
			$max_order_id++;
			
			Db::execute('UPDATE programms_orders SET id=?, total_transactions = total_transactions+1 WHERE id=?',$max_order_id,$order_id);			
			
			$order_id = $max_order_id;
		}
		
		$endPoint = $this->getEndPoint('registerOrder');
		$amount = Db::execute("SELECT amount, usdRate, eurRate, currency FROM programms_orders WHERE id=?", $order_id)->getArray();
		
		//переводим стоимость в рубли по курсу на момент создания заказа		
		if($amount[0]['currency'] == 'usd')
			$amount_in_rub = $amount[0]['amount']*$amount[0]['usdRate'];
		else
			$amount_in_rub = $amount[0]['amount']*$amount[0]['eurRate'];
					
		//значение суммы передается в копейках
		$amount_in_rub *= 100;
		
		$url = $this->server.$endPoint
		.'?userName='.$this->userName
		.'&password='.$this->pass
		.'&amount='.$amount_in_rub
		.'&orderNumber='.$order_id
		.'&currency=643'
		.'&returnUrl='.conf('site.home').'/programms/discount/?succPayment'
		.'&failUr='.conf('site.home').'/programms/discount/?errorPayment';
						
		$data = $this->curlGetPage($url);
						
		if(isset($data['orderId']) && isset($data['formUrl']) && $data['formUrl']!=""){
			$paymentUrl = $data['formUrl'];
			
			//lock order
			Db::execute('UPDATE programms_orders SET status="Processing", transaction_id=? WHERE id=?',$data['orderId'],$order_id);
		}
		else{
			//$data['errorCode']
			$paymentUrl = '/programms/discount/?errorCode='.(isset($data['errorCode'])?$data['errorCode']:"-1");
		}
		
		return $paymentUrl;
	}
	
	function getOrderStatus($order_id){
		$data = array();
		
		$endPont = $this->getEndPoint('orderStatus');
		
		$url = $this->server.$endPont
		.'?userName='.$this->userName
		.'&password='.$this->pass
		.'&orderNumber='.$order_id;
		
		$data = $this->curlGetPage($url);
				
		return $data;
	}
	
	function curlGetPage($url){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$result = curl_exec($ch);
		curl_close($ch);
		
		return json_decode($result,true);
	}
	
	function getErrorDetails($errorCode){
		$details = "Обработка запроса прошла без системных ошибок";
		
		switch($errorCode){
			case 1: $details = "Заказ с таким номером уже зарегистрирован в системе"; break;
			case 3: $details = "Неизвестная (запрещенная) валюта"; break;
			case 4: $details = "Отсутствует обязательный параметр запроса"; break;
			case 5: $details = "Ошибка значение параметра запроса"; break;
			case 7: $details = "Системная ошибка"; break;
		}
		
		return $details;
	}
}
?>